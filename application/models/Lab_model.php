<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lab_model extends CI_Model {

	var $table = 'lab';

	public function get_Lab(){
		return $this->db->get($this->table)->result();
	}	

	public function add_Lab($data){
		return $this->db->insert($this->table, $data);
	}

	public function find_by_id($id){
		return $this->db->from($this->table)
				->where('id_lab', $id)
				->get()
				->row();
	}

	public function update($id,$data){
		return $this->db->where('id_lab', $id)
			->update($this->table, $data);
	}

	public function delete($id){
		return $this->db->where('id_lab', $id)
					->delete($this->table);
	}
}