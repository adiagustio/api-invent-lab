<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_model extends CI_Model {

	var $table = 'barang';

	public function get_barang(){
		return $this->db->from('barang')
			->join('lab', 'barang.id_lab = lab.id_lab', 'left')
			->get()
			->result_array();
	}


	function add_brg($data){
		return $this->db->insert($this->table, $data);
	}


	public function find_by_id($id){
		return $this->db->from($this->table)
				->where('id', $id)
				->get()
				->row();
	}

	public function update($id,$data){
		return $this->db->where('id', $id)
			->update($this->table, $data);
	}

	public function delete($id){
		return $this->db->where('id', $id)
					->delete($this->table);
	}


}