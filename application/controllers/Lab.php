<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');

class Lab extends CI_Controller {

	public function index(){
		$data = $this->Lab_model->get_Lab();
		$this->output
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($data, JSON_PRETTY_PRINT));
	}

	public function find($id)
	{
		$Lab = $this->Lab_model->find_by_id($id);

		if($Lab){
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($Lab, JSON_PRETTY_PRINT));
		}else{
			$data = ['message' => 'Data Lab tidak ada', 'status' => false];
			$this->output->set_content_type('application/json')->set_output(json_encode($data, JSON_PRETTY_PRINT));
		}	
	}

	public function add()
	{
		$data = $this->input->post();
		$result = $this->Lab_model->add_Lab($data);

		if($result != true){
			$data = ['message' => 'Data Lab gagal disimpan', 'status' => false]; 
			$this->output
				->set_content_type('application/json')
				->set_output(json_encode($data, JSON_PRETTY_PRINT));

		}else{
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($data, JSON_PRETTY_PRINT));
		}
	}

	public function update($id){
		$request = $this->input->post();
		$result = $this->Lab_model->update($id,$request);

		if($result != true){
			$data = ['message' => 'Data Lab gagal disimpan', 'status' => false]; 
			$this->output
				->set_content_type('application/json')
				->set_output(json_encode($data, JSON_PRETTY_PRINT));
		}else{
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($request, JSON_PRETTY_PRINT));
		}
	}

	public function delete($id){		
		$result = $this->Lab_model->delete($id);
		if($result != true){
			$data = ['message' => 'Data Lab gagal dihapus', 'status' => false];
			$this->output
				->set_content_type('application/json')
				->set_output(json_encode($data, JSON_PRETTY_PRINT));
		}else{
			$data = ['message' => 'Data Lab berhasil dihapus'];
			$this->output
				->set_content_type('application/json')
				->set_output(json_encode($data, JSON_PRETTY_PRINT));
		}
	}

}