<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');

class Barang extends CI_Controller {

	public function index(){
		$barang = $this->Barang_model->get_barang();
		$this->output
			->set_content_type('application/json', 'utf-8')
			->set_status_header('200')
			->set_output(json_encode($barang, JSON_PRETTY_PRINT));
	}

	public function find($id){
		$brg = $this->Barang_model->find_by_id($id);
		if(!$brg){
			$this->output->set_content_type('application/json')
			->set_output(json_encode(["message" => "Barang yang Anda cari tidak ada", "status" => false]));
		}else{
			$this->output->set_content_type('application/json','utf-8')
				->set_status_header('200')
				->set_output(json_encode($brg, JSON_PRETTY_PRINT));	
		}
	}

	public function add()
	{
		$data = $this->input->post();
		$result = $this->Barang_model->add_brg($data);

		if($result != true){
			$data = ['message' => 'Data Barang gagal disimpan','status' => false]; 
			$this->output
				->set_content_type('application/json')
				->set_output(json_encode($data, JSON_PRETTY_PRINT));

		}else{
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($data, JSON_PRETTY_PRINT));
		}
	}

	public function update($id){
		$request = $this->input->post();
		$result = $this->Barang_model->update($id,$request);

		if($result != true){
			$data = ['message' => 'Data Barang gagal disimpan', 'status' => false]; 
			$this->output
				->set_content_type('application/json')
				->set_output(json_encode($data, JSON_PRETTY_PRINT));
		}else{
			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($request, JSON_PRETTY_PRINT));
		}
	}

	public function delete($id){		
		$result = $this->Barang_model->delete($id);
		if($result != true){
			$data = ['message' => 'Data Barang Gagal Dihapus', 'status' => false];
			$this->output
				->set_content_type('application/json')
				->set_output(json_encode($data, JSON_PRETTY_PRINT));
		}else{
			$data = ['message' => 'Data Barang berhasil dihapus'];
			$this->output
				->set_content_type('application/json')
				->set_output(json_encode($data, JSON_PRETTY_PRINT));
		}
	}

}
